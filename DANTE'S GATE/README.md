### DANTE'S GATE MINIMAL VERSION:
Herramienta para extracción de datos.
Repositorio del conjunto de herramientas para hacer OSINT y SOCMINT con Dante's Gates Minimal Version.
Jorge Coronado
github.com/quantika14
https://github.com/Quantika14/osint-suite-tools

#### ¿Cómo funciona?
Para su funcionamiento 100% usar sistemas Linux o Mac
* Tener instalado Python 2.7
* Instalar PDFGREP sudo apt-get install pdfgrep
* Instalar las dependencias pip install -r requeriments.txt
Para ejecutar cualquier buscador solo tendremos que hacer por ej: python BuscadorPersonas.py
Seleccionar las diferentes configuraciones e insertar los datos que nos pida
Enjoy!

#### Requisitos
Tener instalado Python 2.7
Tener instalado PDFGREP
Tener instalado las librerias descritas en requeriments.txt



#### VARIOS:
* OSINT:
https://en.wikipedia.org/wiki/Open-source_intelligence
Open-source intelligence (OSINT) is data collected from publicly available sources to be used in an intelligence context.[1]
In the intelligence community, the term "open" refers to overt, publicly available sources (as opposed to covert or clandestine sources). It is not related to open-source software or public intelligence.

* MEETUP:
OSINT, OSANT cada día te quiero más 2.0: > investigación > usando Dante's Gates
https://www.meetup.com/es-ES/HackMadrid-27/events/259013142/
