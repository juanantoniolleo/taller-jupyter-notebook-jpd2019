Mark_Down-formato_MD:
* NOTAS Y APUNTES SOBRE EDICION Y RECURSOS EN FORMATO Mark Down
--------------------------------------------------------------------------------
* Markdown_WP_EN.pdf

* WEB:
https://daringfireball.net/projects/markdown/

Heading
=======

* ENCABEZAMIENTOS:
	* ENTRE # Y #### PARA LOS ENCABEZAMIENTOS

## Sub-heading
 
 
* PARRAFOS:
Paragraphs are separated
by a blank line.

* SALTO DE LINEA:

Two spaces at the end of a line  
leave a line break.

* ATRIBUTOS DE TEXTO:

Text attributes _italic_, 
**bold**, `monospace`.

* LINEA SEPARADORA:

Horizontal rule:

---

* LISTA CON BOLA:

Bullet list:

  * apples
  * oranges
  * pears
  
* LISTA CON NUMEROS:
  
Numbered list:

  1. wash
  2. rinse
  3. repeat
  
* ENLACES
  
A [link](http://example.com).

* IMAGEN

![Image](Image_icon.png)

* MARKDOWN USA EL ESTILO DEL CORREO ELECTRÓNICO PARA BLOCKQUOTING???
> Markdown uses email-style > characters for blockquoting.

* ?????????
Inline <abbr title="Hypertext Markup Language">HTML</abbr> is supported.

* TABLAS
This is a regular paragraph.

<table>
    <tr>
        <td>Foo</td>
    </tr>
</table>

This is another regular paragraph


--------------------------------------------------------------------------------
